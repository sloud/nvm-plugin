export NVM_DIR="$HOME/.nvm"

__nvm_init() {
    source "$NVM_DIR/nvm.sh"

    autoload -U add-zsh-hook
    load-nvmrc() {
        local node_version="$(nvm version)"
        local nvmrc_path="$(nvm_find_nvmrc)"

        if [ -n "$nvmrc_path" ]; then
            local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

            if [ "$nvmrc_node_version" = "N/A" ]; then
                nvm install
            elif [ "$nvmrc_node_version" != "$node_version" ]; then
                nvm use
            fi
        elif [ "$node_version" != "$(nvm version default)" ]; then
            echo "Reverting to nvm default version"
            nvm use default
        fi
    }
    add-zsh-hook chpwd load-nvmrc
    load-nvmrc
}

if [ -d "$NVM_DIR" ] && [ -s "$NVM_DIR/nvm.sh" ]; then
    __nvm_init
else
    rm -rf "$NVM_DIR"
    mkdir -p "$HOME/.cache"
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh >"$HOME/.cache/nvm-install.sh"
    PROFILE=/dev/null bash "$HOME/.cache/nvm-install.sh"

    __nvm_init
    nvm install --lts --default
    nvm install-latest-npm
fi
